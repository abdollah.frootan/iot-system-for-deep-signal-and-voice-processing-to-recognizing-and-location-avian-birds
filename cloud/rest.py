import cherrypy
import json
import time 
import redis
from redis.commands.json import path


#connecting to the redis server 
REDIS_HOST = 'redis-10576.c1.us-east1-2.gce.cloud.redislabs.com'
REDIS_PORT = 10576
REDIS_USERNAME = 'default'
REDIS_PASSWORD = '0AtWdT6cTaam1vOCBicgumO9EqBVVWL1'


redis_client = redis.Redis(host=REDIS_HOST, port=REDIS_PORT, username=REDIS_USERNAME, password=REDIS_PASSWORD)
is_connected = redis_client.ping()

#/sensors
class sensors(object):
    exposed = True
    def GET(self, *path, **query):
        #path length have to be 0
        
            
        if len(path) >1 : 
            print(path)
            raise cherrypy.HTTPError(400,'Bad Request: path is wrong.')
        
        from_ts = query.get('from',None)
        to_ts = query.get('to',None)
        
        items= []
        keys = redis_client.keys()
        for key in keys:
            key = key.decode()
            
            sensor_dict={
                'id': key.split(":")[0],
                'name': key.split(":")[1],
                'description':key.split(":")[4],
                'latitude':key.split(":")[2],
                'longitude':key.split(":")[3],
                'lastDetection':str(redis_client.ts().get(key)[0])
            }

            if from_ts is not None and to_ts is not None:
                key_existance = redis_client.ts().range(key, from_ts, to_ts)
                #if key-existance list is empty it means there is not any data on that preiod for the sensor so we do not add sensor to our returning items
                if len(key_existance) !=0 : 
                    items.append(sensor_dict)
            elif from_ts is not None:
                raise cherrypy.HTTPError(400,'Bad Request: missing end point.')
            elif to_ts is not None:
                raise cherrypy.HTTPError(400,'Bad Request: missing start point.')
            else:
                print("all items returns")
                items.append(sensor_dict)

        response =  json.dumps(items)
        return response 


#/sensor
class deviceOp(object):
    exposed = True

    def GET(self, *path, **query):

        if len(path) != 1: 
            raise cherrypy.HTTPError(400,'Bad Request: missing Id address or path is wrong.')
        id= str(path[0])
        keys = redis_client.keys()
        ids=[]
        
        for key in keys:
            key = key.decode()

            sensor_dict={
                'id': key.split(":")[0],
                'name': key.split(":")[1],
                'description':key.split(":")[4],
                'latitude':key.split(":")[2],
                'longitude':key.split(":")[3],
                'lastDetection':str(redis_client.ts().get(key)[0])
            }
            print(key)
            print(sensor_dict)
            print("############")
            ids.append(sensor_dict['id'])
            if id == key.split(":")[0]:
                item =sensor_dict
        if id  not in ids:
            raise cherrypy.HTTPError(404,'Not Found: invalid MAC address.')
        response =  json.dumps(item)
        return response 

    def DELETE(self, *path, **query):
        if len(path) != 1: 
            raise cherrypy.HTTPError(400,'Bad Request: missing Id.')
        id= str(path[0])
        keys = redis_client.keys()
        ids=[]
        
        for key in keys:
            key = key.decode()
            sensor_dict={
                'id': key.split(":")[0],
                'name': key.split(":")[1],
                'description':key.split(":")[4],
                'latitude':key.split(":")[2],
                'longitude':key.split(":")[3],
                'lastDetection':str(redis_client.ts().get(key)[0])
            }
            ids.append(sensor_dict['id'])
            if id == key.split(":")[0]:
                item =key
        if id  not in ids:
            raise cherrypy.HTTPError(404,'Not Found: invalid Id.')

        redis_client.delete(item)
        return


#/reading
class reader(object):
    exposed = True

    def GET(self, *path, **query):
        current_time=int(time.time() * 1000)

        if len(path) != 1: 
            raise cherrypy.HTTPError(400,'Bad Request: missing Id address or path is wrong.')
        id= str(path[0])
        keys = redis_client.keys()
        ids=[]
        from_ts = query.get('from',None)
        to_ts = query.get('to',None)
        
        for key in keys:
            key = key.decode()

            sensor_dict={
                'id': key.split(":")[0],
                'name': key.split(":")[1],
                'description':key.split(":")[4],
                'latitude':key.split(":")[2],
                'longitude':key.split(":")[3],
                'lastDetection':str(redis_client.ts().get(key)[0])
            }
            
            ids.append(sensor_dict['id'])
            if id == sensor_dict['id']:
                item_key=key
                item_data=sensor_dict
        if id not in ids:
            raise cherrypy.HTTPError(404,'Not found: Id is not exist.')
        #in case it exist we check that if start and end point is specified or not
        if from_ts is not None and to_ts is not None:
            timeserie=redis_client.ts().range(item_key, int(from_ts), int(to_ts))
            timestamps=[]
            values=[]
            for t , value in timeserie:
                timestamps.append(t)
                values.append(value)
            item={
                'id':id,
                'name':item_data['name'],
                'description':item_data['description'],
                'timestamp':timestamps
            }
        elif from_ts is not None:
            raise cherrypy.HTTPError(400,'Bad Request: missing end point.')
        elif to_ts is not None :
            raise cherrypy.HTTPError(400,'Bad Request: missing start point.')
        else:
            #its the situation that boath are None so
            timeserie=redis_client.ts().range(item_key, 0, current_time ) #all time
            timestamps=[]
            values=[]
            for t , value in timeserie:

                timestamps.append(t)
                values.append(value)
            item={
                'id':id,
                'name':item_data['name'], #we can add any other thing to return from item_data dictionary
                'timestamp':timestamps
            }
        response =  json.dumps(item)
        
        return response


if __name__ == '__main__':
    conf = {'/': {'request.dispatch': cherrypy.dispatch.MethodDispatcher()}}
    cherrypy.tree.mount(sensors(), '/sensors', conf)
    cherrypy.tree.mount(deviceOp(), '/sensor', conf) 
    cherrypy.tree.mount(reader(), '/readings', conf) 

    cherrypy.config.update({'server.socket_host': '0.0.0.0'})
    cherrypy.config.update({'server.socket_port': 8080})
    cherrypy.engine.start()
    cherrypy.engine.block()