import tensorflow as tf
import tensorflow_io as tfio

def get_padded_audio(audio, sampling_rate):
    zero_padding = tf.zeros(tf.math.mod(tf.shape(audio), sampling_rate), dtype=tf.float32)
    audio_padded = tf.concat([audio, zero_padding], axis=0)
    return audio_padded

def get_audio_from_numpy(indata, sampling_rate, padding = False) -> tf.Tensor:
        indata = tf.convert_to_tensor(indata, dtype=tf.float32)
        indata = 2 * ((indata + 32768) / (32767 + 32768)) - 1
        indata = tf.squeeze(indata)
        if padding:
            zero_padding = tf.zeros(tf.math.mod(tf.shape(indata), sampling_rate), dtype=tf.float32)
            indata = tf.concat([indata, zero_padding], axis=0)
        return indata

def get_spectrogram(audio, sampling_rate, downsampling_rate, frame_length_in_s, frame_step_in_s) -> tf.Tensor:
    if downsampling_rate != sampling_rate:
        sampling_rate_int64 = tf.cast(sampling_rate, tf.int64)
        audio = tfio.audio.resample(audio, sampling_rate_int64, downsampling_rate)

    sampling_rate_float32 = tf.cast(downsampling_rate, tf.float32)
    frame_length = int(frame_length_in_s * sampling_rate_float32)
    frame_step = int(frame_step_in_s * sampling_rate_float32)

    spectrogram = stft = tf.signal.stft(
        audio, 
        frame_length=frame_length,
        frame_step=frame_step,
        fft_length=frame_length
    )
    spectrogram = tf.abs(stft)
    return spectrogram

def get_log_mel_spectrogram(indata, sampling_rate, downsampling_rate, frame_length_in_s, frame_step_in_s, num_mel_bins, lower_frequency, upper_frequency) -> tf.Tensor:
    spectrogram = get_spectrogram(indata, sampling_rate, downsampling_rate, frame_length_in_s, frame_step_in_s)

    sampling_rate_float32 = tf.cast(downsampling_rate, tf.float32)
    frame_length = int(frame_length_in_s * sampling_rate_float32)
    num_spectrogram_bins = frame_length // 2 + 1

    linear_to_mel_weight_matrix = tf.signal.linear_to_mel_weight_matrix(
        num_mel_bins=num_mel_bins,
        num_spectrogram_bins=num_spectrogram_bins,
        sample_rate=downsampling_rate,
        lower_edge_hertz=lower_frequency,
        upper_edge_hertz=upper_frequency
    )

    mel_spectrogram = tf.matmul(spectrogram, linear_to_mel_weight_matrix)

    log_mel_spectrogram = tf.math.log(mel_spectrogram + 1.e-6)
    return log_mel_spectrogram

def get_mfccs(indata, sampling_rate, downsampling_rate, frame_length_in_s, frame_step_in_s, num_mel_bins, lower_frequency, upper_frequency, num_coefficients) -> tf.Tensor:
    log_mel_spectrogram = get_log_mel_spectrogram(indata, sampling_rate, downsampling_rate, frame_length_in_s, frame_step_in_s, num_mel_bins, lower_frequency, upper_frequency)
    mfccs = tf.signal.mfccs_from_log_mel_spectrograms(log_mel_spectrogram)
    mfccs = mfccs[..., :num_coefficients]
    return mfccs

def is_silence(indata, sampling_rate, downsampling_rate, frame_length_in_s, dbFSthres, duration_thres) -> bool:
    spectrogram = get_spectrogram(
        indata,
        sampling_rate,
        downsampling_rate,
        frame_length_in_s,
        frame_length_in_s
    )
    dbFS = 20 * tf.math.log(spectrogram + 1.e-6)
    energy = tf.math.reduce_mean(dbFS, axis=1)
    non_silence = energy > dbFSthres
    non_silence_frames = tf.math.reduce_sum(tf.cast(non_silence, tf.float32))
    non_silence_duration = (non_silence_frames + 1) * frame_length_in_s

    if non_silence_duration > duration_thres:
        return False
    else:
        return True