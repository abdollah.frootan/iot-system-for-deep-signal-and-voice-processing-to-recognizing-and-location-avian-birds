from dataclasses import dataclass
import multiprocessing
import os
import time
import uuid

import tensorflow as tf
import tensorflow_io as tfio

import sounddevice as sd

import paho.mqtt.client as mqtt
import geocoder
import json

import argparse
import glob
from functools import partial

import audio_utils

# Supressing INFO tensorflow logs
os.environ['TF_CPP_MIN_LOG_LEVEL']='3'
# Supressing tensorflow warnings
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.FATAL)

# ===== ARGUMENTS =====

# Read arguments from CMD input
parser= argparse.ArgumentParser()
parser.add_argument('--name', help="Sensor's name", required = False, type=str)
parser.add_argument('--description', help="Sensor's description", required = False, type=str)
parser.add_argument('--location', help="Sensor latitude and longitude, if not provided it will be taken from GPS", required = False, type=float, nargs=2)
parser.add_argument('--device', help="ID of the device used to record the audio.", required = True, type=int)
parser.add_argument('--model', help="Tensorflow Lite model file.", required = True, type=str)
parser.add_argument('--sr', help="Recorder sampling rate.", required = True, type=int)
parser.add_argument('--mqtt_broker', help="MQTT broker address", required = True, type=str)
parser.add_argument('--mqtt_port', help="MQTT broker port", required = True, type=int)
parser.add_argument('--mqtt_topic', help="MQTT topic to send the messages", required = True, type=str)
args = parser.parse_args()

SENSOR_NAME = args.name
SENSOR_DESC = args.description
SENSOR_LOCATION = args.location

INPUT_DEVICE = args.device
MODEL_FILE = args.model
RECORDING_SAMPLING_RATE = args.sr

MQTT_BROKER = args.mqtt_broker
MQTT_PORT = args.mqtt_port
MQTT_TOPIC = args.mqtt_topic

IS_SILENCE_ARGS = {
    'downsampling_rate': RECORDING_SAMPLING_RATE,
    'frame_length_in_s': 0.04,
    'dbFSthres': -130,
    'duration_thres': 0.51
}

MFCCS_ARGS = {
    'downsampling_rate': 24000,
    'frame_length_in_s': 0.05,
    'frame_step_in_s': 0.0334,
    'num_mel_bins': 20,
    'lower_frequency': 1000,
    'upper_frequency': 9000,
    'num_coefficients': 25
}

RECORDING_ARGS = {
    'device': INPUT_DEVICE,
    'channels': 1,
    'dtype': 'int32',
    'samplerate': RECORDING_SAMPLING_RATE,
    'blocksize': int(RECORDING_SAMPLING_RATE / 2),
}

# ===== CLASSES =====

class AudioRecorder:
    def start_record_audio(self, callback_function) -> None:
        try:
            with sd.InputStream(**RECORDING_ARGS, callback=callback_function):
                print('Recording: True')
                while True:
                    pass
        except ValueError as e:
            print(e)

class AvianDetectionModel:

    def __init__(self, model_file):
        self.interpreter = tf.lite.Interpreter(model_path=glob.glob(model_file)[0])
        self.interpreter.allocate_tensors()

        self.input_details = self.interpreter.get_input_details()
        self.output_details = self.interpreter.get_output_details()

        print(f'Model Initialized  -  File: {model_file}')

    def get_audio_state_probabilities(self, audio_data, s_rate):
        mfccs = audio_utils.get_mfccs(audio_data, s_rate, **MFCCS_ARGS)
        mfccs = tf.expand_dims(mfccs, 0)
        mfccs = tf.expand_dims(mfccs, -1)
        mfccs = tf.image.resize(mfccs, [32, 32])

        self.interpreter.set_tensor(self.input_details[0]['index'], mfccs)
        self.interpreter.invoke()
        output = self.interpreter.get_tensor(self.output_details[0]['index'])

        return output[0]
    
class MQTTPublisher:
    def __init__(self, mqtt_broker, mqtt_port, publisher_topic):
        # Create a new MQTT client and connect
        self.client = mqtt.Client() 
        self.client.connect(mqtt_broker, mqtt_port)
        
        self.publisher_topic = publisher_topic

    def publish(self, message: dict):
        mqtt_msg=json.dumps(message)
        self.client.publish(self.publisher_topic, mqtt_msg)
        print(f'Message published: {mqtt_msg}')

# ===== ASYNC TASKS =====

def audio_record_task(process_queue):
    model = AvianDetectionModel(MODEL_FILE)
    mqtt_publisher = MQTTPublisher(MQTT_BROKER, MQTT_PORT, MQTT_TOPIC)
    recorder = AudioRecorder()

    def create_detection_msg(accuracy: float):
        mac_address = hex(uuid.getnode())
        sensor_location = SENSOR_LOCATION
        if sensor_location == None:
            sensor_location = geocoder.ip('me').latlng
        return {
                'id':mac_address ,
                'name': SENSOR_NAME,
                'description': SENSOR_DESC,
                'latitude': str(sensor_location[0]),
                'longitude': str(sensor_location[1]),
                'timestamp': int(time.time() * 1000),
                'accuracy': str(accuracy)
            }

    def callback(indata, frames, callback_time, status):
        audio_data = audio_utils.get_audio_from_numpy(indata, RECORDING_SAMPLING_RATE)
        if not audio_utils.is_silence(audio_data, RECORDING_SAMPLING_RATE, **IS_SILENCE_ARGS):
            audio_data = audio_utils.get_audio_from_numpy(indata, RECORDING_SAMPLING_RATE, True)
            prob = model.get_audio_state_probabilities(audio_data, RECORDING_SAMPLING_RATE)

            bird_prob = prob[1]
            if bird_prob > 0.95:
                msg = create_detection_msg(bird_prob)
                mqtt_publisher.publish(msg)

    # Send one detection at the beginning to mark the sensor as alive
    alive = create_detection_msg(0.0)
    mqtt_publisher.publish(alive)
    
    recorder.start_record_audio(callback)

# ===== MAIN FUNCTION =====

if __name__ == "__main__":
    print('########################################')
    print('Avian flight call detection')
    print('########################################')

    q = multiprocessing.Queue()

    p1 = multiprocessing.Process(target=audio_record_task, args=(q,))
    p1.start()
    p1.join()
    